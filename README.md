
# My Recipe Box

[![N|Solid](https://quitiweb.com/static/qwsite/img/QW_icon.png)](https://www.quitiweb.com)


My Recipe Box is a service that stores the recipes of users, including their scores.
Users can sign up, log in, record their recipes and score them. Every recipe contains two scores: tastiness and ease of cooking, from 1 to 10. Each recipe can be added, edited and deleted. Recipes will be sorted by the average of their two scores in descending order. Every API call will return five recipes. When not using the service, users can log out of it.


This is a Django REST API with a SQLite3 database. This small API implements the next features:

    - Sign-up page to create an account
    - Log-in page -> after logging in, the user is redirected to the Recipe view page.
    - Recipe view page (read-only recipes of the logged-in user, paginated, 5 recipes per page), offers the option to log out
    - API endpoint(s) to create/edit/delete recipes. You may assume that these would work with token authentication so that an external mobile app could query them.


# Table of contents

* [Installation](#Installation)
* [Packages](#Packages)
* [How-to](#How to use My Recipe Box)


### Installation


Download the git repository from: git clone https://Quitiweb@bitbucket.org/Quitiweb/myrecipebox.git
Create a virtual-environment.
Install the dependencies and start the server.


```sh
$ pip install -r requirements.txt
$ py manage.py runserver
```


### Packages

My Recipe Box is currently extended with the following packages.

| Package | Description |
| ------ | ------ |
| BootStrap Admin | Responsive Theme for Django Admin With Sidebar Menu |
| Django Rest Auth | This app makes it easy to build Django powered SPA's (Single Page App) or Mobile apps exposing all registration and authentication related functionality as CBV's (Class Base View) and REST (JSON) |



### How to use My Recipe Box

##### Web Access Usage

 - Go to the main url. If you are in development mode, go to http://127.0.0.1:8000/
 - Click on Login
 - Insert your credentials and click on Login button
 - Click on Get button to check your recipes ordered by both scores descending
 - Fill the fields in order to create a new Recipe and click on Post

##### Web Admin Access
 - Go to the main url /admin. If you are in development mode, go to http://127.0.0.1:8000/admin/
 - Insert your credentials and click on Login button
 - You are able to manage the whole API
 - Create an Auth Token for each user
 - Create, modify and/or delete users
 - Create, modify and/or delete recipes

##### API Access
 - It is possible to make GETs and POSTs to retreive recipes and create new ones
 - There are some examples:
	 - test-get.py
	 - test-post.py
 - You will need your user Token in order to do it
