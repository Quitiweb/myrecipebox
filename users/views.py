from django.urls import reverse_lazy
from django.views import generic
from rest_framework import generics, permissions, authentication
from rest_framework.response import Response
from django.db.models import F, FloatField
from django.db.models.functions import Cast

from .forms import CustomUserCreationForm
from .models import Recipe
from .serializers import RecipeSerializer


class SignUp(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    # template_name = 'signup.html'


class Recipes(generics.ListCreateAPIView):
    queryset = Recipe.objects.all()
    serializer_class = RecipeSerializer
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated, )

    def get(self, request):
        queryset = Recipe.objects.filter(user=request.user).annotate(
            avg_score=Cast((F('tastiness') + F('ease_of_cooking'))/2.0, FloatField())
        ).order_by('-avg_score')[:5]
        serializer = RecipeSerializer(queryset, many=True)
        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
