from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('', views.Recipes.as_view(), name='recipes'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
