from rest_framework import serializers

from .models import Recipe


class RecipeSerializer(serializers.ModelSerializer):
    # user = serializers.Field(source='user.username')
    # user = serializers.SlugRelatedField(slug_field='username', read_only=True)

    class Meta:
        model = Recipe
        fields = ('user', 'recipe_title', 'recipe_text', 'tastiness', 'ease_of_cooking', )
