from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.core.validators import RegexValidator, MaxValueValidator, MinValueValidator

MED_LENGTH = 500
MAX_LENGTH = 1500


class CustomUserManager(UserManager):
    pass


class CustomUser(AbstractUser):
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Formato: '+999999999'. Max 15 dígitos.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)  # validators should be a list
    address = models.CharField(max_length=MED_LENGTH, blank=True)
    personal_info = models.TextField(max_length=MAX_LENGTH, blank=True)

    objects = CustomUserManager()

    def __str__(self):
        return self.username


class Recipe(models.Model):
    user = models.ForeignKey('CustomUser', on_delete=models.CASCADE, related_name='recipes',
                             limit_choices_to={'is_superuser': False})
    recipe_title = models.CharField(max_length=MED_LENGTH)
    recipe_text = models.TextField(max_length=MAX_LENGTH, blank=True)
    tastiness = models.IntegerField(default=5, validators=[MaxValueValidator(10), MinValueValidator(1)])
    ease_of_cooking = models.IntegerField(default=5, validators=[MaxValueValidator(10), MinValueValidator(1)])
