import os
import requests

# url = 'http://127.0.0.1:8000/api/v1/1/'
url = "http://127.0.0.1:8000"
# resp = requests.get(url)
# assert resp.status_code == 403

# api_key = 'xbgLK0wM.NE4gL4BLFBRX7ZUkhCH0ziMT1lK6R3m5'
# auth = f"Api-Key {api_key}"
api_key = "2fa45a16b2248a5b5feb428186607ce08347f1a7"
auth = f"Token {api_key}"

resp = requests.get(url, headers={"Authorization": auth})
# assert resp.status_code == 200

print(resp.json())
# print(resp)

# curl -X GET http://127.0.0.1:8000/api/example/ -H 'Authorization: Token 2fa45a16b2248a5b5feb428186607ce08347f1a7'
