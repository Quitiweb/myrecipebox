bootstrap-admin==0.4.3
Django==2.2.5
django-filter==2.2.0
django-rest-auth==0.9.5
djangorestframework==3.10.2
pytz==2019.2
six==1.12.0
sqlparse==0.3.0
