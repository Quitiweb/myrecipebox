import requests

url = "http://127.0.0.1:8000"

api_key = "2fa45a16b2248a5b5feb428186607ce08347f1a7"
auth = f"Token {api_key}"

code = {'user': 2, 'recipe_title': 'Recipe Seven', 'recipe_text': 'Testing', 'tastiness': 9, 'ease_of_cooking': 4}

resp = requests.post(url, code, headers={"Authorization": auth})

print(resp.json())
# print(resp)
